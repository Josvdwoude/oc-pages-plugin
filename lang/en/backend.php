<?php

return [
	'category_name' => 'Autumn',
	'side_pages'    => 'Pages',

	'plugin_name'       => 'Autumn pages',
	'rule_access_pages' => 'Manage Pages',
	'create_page_title'	=> 'Create page',
	'edit_page_title'   => 'Edit page',
	'preview_page_title'=> 'Preview',


	'pages_title'  => 'Title',
	'pages_slug'   => 'Page Slug',
	'pages_created'=> 'Created',
	'pages_updated'=> 'Updated',

	'new_page_button' => 'New Page',

	'page_form_title_label' => 'Title',
	'page_form_title_placeholder' => 'New post title',
	'page_form_title_hint'  => 'Page title',

	'page_form_slug_label'  => 'Slug',
	'page_form_slug_hint'   => 'Page url identifier',

	'page_form_seo_descr_label'  => 'SEO Description',
	'page_form_seo_descr_hint'   => 'Used in metadata description',

	'page_form_seo_keys_label'  => 'SEO Keywords',
	'page_form_seo_keys_hint'   => 'Used in metadata keywords',

	'page_form_content_label'  => 'Content',

	'page_form_images_label'  => 'Images',
];