<?php

return [
	'category_name' => 'Autumn',
	'side_pages'    => 'Страницы',

	'plugin_name'       => 'Autumn pages',
	'rule_access_pages' => 'Управление страницами',
	'create_page_title'	=> 'Новая страница',
	'edit_page_title'   => 'Редактирование страницы',
	'preview_page_title'=> 'Предпросмотр',


	'pages_title'  => 'Заголовок',
	'pages_slug'   => 'Slug',
	'pages_created'=> 'Создана',
	'pages_updated'=> 'Обновлена',

	'new_page_button' => 'Создать страницу',

	'page_form_title_label' => 'Заголовок',
	'page_form_title_placeholder' => 'Заголовок страницы',
	'page_form_title_hint'  => 'Заголовок страницы',

	'page_form_slug_label'  => 'Slug',
	'page_form_slug_hint'   => 'Индетификатор в url',

	'page_form_seo_descr_label'  => 'SEO описание',
	'page_form_seo_descr_hint'   => 'Будет добавлено в мета теги страницы',

	'page_form_seo_keys_label'  => 'SEO ключевые слова',
	'page_form_seo_keys_hint'   => 'Будет добавлено в мета теги страницы',

	'page_form_content_label'  => 'Текст страницы',

	'page_form_images_label'  => 'Изображения',
];